<div class="header_bar">
    
    
    <!-- Branding -->
    <div class="brand">
        <div class="logo" style="display:block"><span class="theme_color">Jetti</span> Gasoline</div>
        <div class="small_logo" style="display:none"><img src="images/s-logo.png" width="50" height="47" alt="s-logo" /> <img src="images/r-logo.png" width="122" height="20" alt="r-logo" /></div>
    </div>
    
    <!-- Top Navigation -->
    <div class="header_top_bar">
        
        <!-- Left Side -->
        @include('partials.header-bar-left')
        
        <!-- Right Side -->
        @include('partials.header-bar-right')
        
    </div>

</div>