<div class="left_nav">
    <!--\\\\\\\left_nav start \\\\\\-->
    <div class="search_bar"> <i class="fa fa-search"></i>
        <input name="" type="text" class="search" placeholder="Search Dashboard..." />
    </div>
    <div class="left_nav_slidebar">
        <ul>
            
            <li class="left_nav_active theme_border"><a href="#"><i class="fa fa-home"></i> DASHBOARD <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
                <ul style="display:block">
                    <li> <a href="#" class="left_nav_sub_active"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Dashboard</b> </a> </li>
                </ul>
            </li>

            <li> <a href="#"> <i class="fa fa-users"></i> Accounts <span class="plus"><i class="fa fa-plus"></i></span></a>
                <ul>
                    <li> <a href="#"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Suppliers</b> </a> </li>
                </ul>
            </li>


            <li> <a href="#"> <i class="fa fa-list-alt"></i> Inventory <span class="plus"><i class="fa fa-plus"></i></span></a>
                <ul>
                    <li> 
                        <a href="#"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Inventory List</b> </a> 
                    </li>
                    <li> 
                        <a href="#"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Categories</b> </a> 
                    </li>
                </ul>
            </li>
            <li> <a href="#"> <i class="fa fa-truck icon"></i> Orders <span class="plus"><i class="fa fa-plus"></i></span> </a>
                <ul>
                    <li> 
                        <a href="#"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Purchase Orders</b> </a> 
                    </li>
                </ul>
            </li>
            
            <li> <a href="#"> <i class="fa fa-truck icon"></i> Sales <span class="plus"><i class="fa fa-plus"></i></span> </a>
                <ul>
                    <li> 
                        <a href="#"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Purchase Orders</b> </a> 
                    </li>
                </ul>
            </li>
            
            <li> <a href="#"> <i class="fa fa-bar-chart-o icon"></i> Reports <span class="plus"><i class="fa fa-plus"></i></span> </a>
                <ul>
                    <li> 
                        <a href="#"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Sales Report</b> </a> 
                    </li>
                  
                </ul>
            </li>
            
        </ul>
    </div>
</div>