<div class="top_right_bar">
    
    
    @include('partials.header-bar-right-addons')
    
    <div class="user_admin dropdown">
        <a href="#" data-toggle="dropdown">
            <i class="fa fa-user"></i><span class="user_adminname">{{ Auth::user()->name }}</span> <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <div class="top_pointer"></div>
            <li> <a href="#"><i class="fa fa-user"></i> Account Details</a> </li>
            <li> <a href="#"><i class="fa fa-cog"></i> System Setting </a></li>
            <li> <a href="{{ url('auth/logout') }}"><i class="fa fa-power-off"></i> Logout</a> </li>
        </ul>
    </div>


    <!-- Toggle the right-side-bar -->
<!--            <a href="javascript:;" class="toggle-menu menu-right push-body jPushMenuBtn rightbar-switch"><i class="fa fa-comment chat"></i></a>-->

</div>