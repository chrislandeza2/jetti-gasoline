<!-- Default Styles needed for our App. -->
<link href="{{ asset('vendor/jetti/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/jetti/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/jetti/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/jetti/css/admin.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('js/main.js') }}"/>