<!DOCTYPE html">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>@yield('page-title')</title>
        <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
        
        @include('partial-styles.default')
        
        @yield('master-styles')
        
    </head>
    
    <body class="light_theme  fixed_header left_nav_fixed">
        
        <div class="wrapper">
            <!--\\\\\\\ wrapper Start \\\\\\-->
            
            
            @include('partials.header-bar')
          
            <div class="inner">
              
                @include('partials.side-bar-left')
                
                <!--\\\\\\\left_nav end \\\\\\-->
                <div class="contentpanel">
                    @yield('content')
                  
                    
                    <!--\\\\\\\ container  end \\\\\\-->
                </div>
                <!--\\\\\\\ content panel end \\\\\\-->
            </div>
            <!--\\\\\\\ inner end\\\\\\-->
        </div>
        <!--\\\\\\\ wrapper end\\\\\\-->
        
        @include('partial-scripts.default')
        @yield('master-scripts')
        
    </body>
</html>
