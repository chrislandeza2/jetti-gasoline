<!-- Default Scripts needed for our App. -->
<script src="{{ asset('vendor/jquery/jquery-1.11.3.min.js') }}"></script>
<script src="{{ asset('vendor/jetti/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/jetti/js/common-script.js') }}"></script>
<script src="{{ asset('vendor/jetti/js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('vendor/jetti/js/jPushMenu.js') }}"></script>
<script src="{{ asset('vendor/jetti/js/side-chats.js') }}"></script>