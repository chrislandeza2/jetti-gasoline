<?php

use Illuminate\Database\Seeder;
use App\DB\User\User;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        if (App::environment() === 'production') {
            exit('I just stopped you getting fired.');
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');


        User::truncate();

        User::create([
            'name' => 'Chris Landeza',
            'email' => 'CC_Landeza@yahoo.com',
            'password' => 'admin.11',
            'username' => 'chrislandeza',
            'role_id' => 1,
            'is_active' => 1
        ]);

        User::create([
            'name' => 'Mico Dela Cruz',
            'email' => 'mico.delacruz@sandmansystem.com',
            'password' => 'admin.11',
            'username' => 'micodelacruz',
            'role_id' => 1,
            'is_active' => 1
        ]);
    }
}